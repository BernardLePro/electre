package mines.ales.electre.modele;

public enum ComparaisonCritere {
    Inexistant(0),
    TresFaible(1),
    Faible(2),
    Moyen(3),
    Fort(4),
    TresFort(5),
    Extreme(6);

    private final float value;

    ComparaisonCritere(float value) {
        this.value = value;
    }
}
