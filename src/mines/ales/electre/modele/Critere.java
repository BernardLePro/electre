package mines.ales.electre.modele;

import mines.ales.electre.modele.comparator.BiggerComparator;
import mines.ales.electre.modele.comparator.Comparator;

public class Critere {

    String nom;
    float poids;
    float seuilDiscordanceBas;
    float seuilDiscordanceHaut;

    /**
     * Par défaut, on suppose que le critère qui a l'a plus grande valeur est le meilleur
     */
    Comparator comparator = new BiggerComparator();

    public String getNom() {
        return nom;
    }

    public Critere(String nom, float poids, float seuilDiscordanceHaut, float seuilDiscordanceBas) {
        this.nom = nom;
        this.poids = poids;
        this.seuilDiscordanceBas = seuilDiscordanceBas;
        this.seuilDiscordanceHaut = seuilDiscordanceHaut;
    }

    public float getPoids() {
        return poids;
    }

    public float getSeuilDiscordanceBas() {
        return seuilDiscordanceBas;
    }

    public float getSeuilDiscordanceHaut() {
        return seuilDiscordanceHaut;
    }

    public void setComparator(Comparator comparator) {
        this.comparator = comparator;
    }

    /**
     * Permet de comparer les valeurs de 2 critères
     *
     * @return le poids si critere1 meilleur que critere2, 0 sinon
     */
    public float compareConcordance(float value1, float value2) {
        if (comparator.compare(value1, value2))
            return poids;
        return 0;
    }

    public float compareDiscordance(float value1, float value2) {
        if (comparator.compare(value1, value2))
            return 0;
        return Math.abs(value2 - value1);
    }
}
