package mines.ales.electre.modele.graphe;

import mines.ales.electre.modele.Action;

public class Noeud implements Comparable {
    Action action;

    public Noeud(Action action) {
        this.action = action;
    }

    public String getNom() {
        return action.getNom();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Noeud noeud = (Noeud) o;

        return !(action != null ? !action.equals(noeud.action) : noeud.action != null);

    }

    @Override
    public int hashCode() {
        return action != null ? action.getNom().hashCode() : 0;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Noeud)
            return getNom().compareTo(((Noeud) o).getNom());
        return 0;
    }

    @Override
    public String toString() {
        return getNom();
    }
}
