package mines.ales.electre.modele.graphe;

import mines.ales.electre.modele.Action;

public class Arc {
    Noeud noeudOrigine;
    Noeud noeudExtremite;

    public Arc(Action noeudOrigine, Action noeudExtremite) {
        this.noeudOrigine = new Noeud(noeudOrigine);
        this.noeudExtremite = new Noeud(noeudExtremite);
    }

    public Arc(Noeud noeudOrigine, Noeud noeudExtremite) {
        this.noeudOrigine = noeudOrigine;
        this.noeudExtremite = noeudExtremite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arc arc = (Arc) o;
        return !(noeudExtremite != null ? !noeudExtremite.equals(arc.noeudExtremite) : arc.noeudExtremite != null) && !(noeudOrigine != null ? !noeudOrigine.equals(arc.noeudOrigine) : arc.noeudOrigine != null);

    }

    @Override
    public int hashCode() {
        int result = noeudOrigine != null ? noeudOrigine.hashCode() : 0;
        result = 31 * result + (noeudExtremite != null ? noeudExtremite.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Arc{" +
                "noeudOrigine=" + noeudOrigine +
                ", noeudExtremite=" + noeudExtremite +
                '}';
    }
}
