package mines.ales.electre.modele.graphe;

import mines.ales.electre.controller.Utils;
import mines.ales.electre.modele.Action;

import java.util.*;

public class Graphe {
    List<Arc> arcs;
    List<Noeud> noeudsSansArcs;

    public void addArc(Arc arc) {
        if (arcs == null)
            arcs = new ArrayList<Arc>();
        arcs.add(arc);
    }

    public void addNoeudsSansArcs(Noeud noeud) {
        if (noeudsSansArcs == null)
            noeudsSansArcs = new ArrayList<Noeud>();
        noeudsSansArcs.add(noeud);
    }

    public List<Noeud> getNoeudsSansArcs() {
        if (noeudsSansArcs == null)
            noeudsSansArcs = new ArrayList<Noeud>();
        return noeudsSansArcs;
    }

    /**
     * Permet de fusionner des noeuds dans le graphe.
     * Attention: détruit les noeuds existants, en crée un nouveau et réassigne les arcs précédents
     *
     * @param noeuds les noeuds à fusionner
     */
    public void fusionnerNoeud(List<Noeud> noeuds) {
        String nom = "";
        for (Noeud noeud : noeuds) {
            nom += noeud.getNom();
        }
        Noeud nouveauNoeud = new Noeud(new Action(nom));
        Set<Arc> arcsAAjouter = new HashSet<Arc>();
        List<Arc> arcsAEnlever = new ArrayList<Arc>();
        for (Arc arc : arcs) {
            if (noeuds.contains(arc.noeudExtremite)) {
                arcsAEnlever.add(arc);
                if (!noeuds.contains(arc.noeudOrigine)) {
                    arcsAAjouter.add(new Arc(arc.noeudOrigine, nouveauNoeud));
                }
            }
            if (noeuds.contains(arc.noeudOrigine)) {
                arcsAEnlever.add(arc);
                if (!noeuds.contains(arc.noeudExtremite)) {
                    arcsAAjouter.add(new Arc(nouveauNoeud, arc.noeudExtremite));
                }
            }
        }
        arcs.removeAll(arcsAEnlever);
        arcs.addAll(arcsAAjouter);
        Utils.logV(getNoeuds());
        Utils.logV(arcs);
    }

    /**
     * @param noeud noeud a supprimé du graphe (supprime les arcs correspondants)
     */
    public void supprimerNoeud(Noeud noeud) {
        List<Arc> arcsASupprimer = new ArrayList<Arc>();
        Noeud noeudTemp;
        for (Arc arc : arcs) {
            if (noeud.equals(arc.noeudExtremite)) {
                noeudTemp = arc.noeudOrigine;
                arcsASupprimer.add(arc);
                //Si un seul arc atteint le noeud, ce noeud deviendra sans arc après suppression
                if (getArcsAtteignantNoeud(noeudTemp).size() == 1) {
                    addNoeudsSansArcs(noeudTemp);
                }
            } else if (noeud.equals(arc.noeudOrigine)) {
                noeudTemp = arc.noeudExtremite;
                arcsASupprimer.add(arc);
                if (getArcsAtteignantNoeud(noeudTemp).size() == 1)
                    addNoeudsSansArcs(noeudTemp);
            }
        }
        if (noeudsSansArcs != null)
            noeudsSansArcs.remove(noeud);
        arcs.removeAll(arcsASupprimer);
    }

    public List<Noeud> getNoeuds() {
        List<Noeud> noeuds;
        HashSet<Noeud> noeuds1 = new HashSet<Noeud>();
        for (Arc arc : arcs) {
            noeuds1.add(arc.noeudExtremite);
            noeuds1.add(arc.noeudOrigine);
        }
        noeuds = new ArrayList<Noeud>(noeuds1);
        return noeuds;
    }


    public List<Arc> getArcsAtteignantNoeud(Noeud noeud) {
        List<Arc> arcsTemp = new ArrayList<Arc>();
        for (Arc arc : arcs) {
            if (arc.noeudExtremite.equals(noeud) || arc.noeudOrigine.equals(noeud))
                arcsTemp.add(arc);
        }
        return arcsTemp;
    }

    /**
     * @return une version String du graphe lisible pour un humain
     */

    public String graph2String() {
        Collections.sort(getNoeuds());
        String graph = "";
        for (Noeud noeud : getNoeuds()) {
            graph += noeud.getNom() + " va vers: ";
            for (Noeud noeud1 : getNoeudsSortant(noeud)) {
                graph += noeud1.getNom() + " ";
            }
            graph += "\nLes noeuds suivants y viennent: ";
            for (Noeud noeud1 : getNoeudsEntrant(noeud)) {
                graph += noeud1.getNom() + " ";
            }
            graph += "\n\n";
        }
        return graph;
    }

    /**
     * @param noeud un noeud du graphe
     * @return tout les noeuds qui possèdent un arc vers le noeud donné
     */
    public List<Noeud> getNoeudsEntrant(Noeud noeud) {
        List<Noeud> noeudList = new ArrayList<Noeud>();
        for (Arc arc : arcs) {
            if (arc.noeudExtremite.equals(noeud)) {
                noeudList.add(arc.noeudOrigine);
            }
        }
        return noeudList;
    }

    public List<Noeud> getNoeudsSortant(Noeud noeud) {
        List<Noeud> noeudList = new ArrayList<Noeud>();
        for (Arc arc : arcs) {
            if (arc.noeudOrigine.equals(noeud)) {
                noeudList.add(arc.noeudExtremite);
            }
        }
        return noeudList;
    }

    /**
     * @return Tout les noeuds qui ne sont pas à l'extrémité d'un arc
     */
    public List<Noeud> getNoeudsSansArcEntrant() {
        List<Noeud> noeudList = new ArrayList<Noeud>();
        for (Noeud noeud : getNoeuds()) {
            if (getNoeudsEntrant(noeud).size() == 0)
                noeudList.add(noeud);
        }
        return noeudList;
    }

    /**
     * @return tout les noeuds qui font parti d'un circuit dans le graphe
     */
    public List<Noeud> getCircuits() {
        List<Noeud> noeudList = new ArrayList<Noeud>();
        for (Noeud noeud : getNoeuds()) {
            if (getSommetsAtteignables(noeud, new HashSet<Noeud>()).contains(noeud))
                noeudList.add(noeud);
        }
        return noeudList;
    }

    /**
     * @param noeud            noeud initial
     * @param noeudDejaAtteint à initialiser par une liste vide au début
     * @return les noeuds que peuvent atteindre le noeud initial
     */
    public Set<Noeud> getSommetsAtteignables(Noeud noeud, Set<Noeud> noeudDejaAtteint) {
        for (Arc arc : arcs) {
            if (arc.noeudOrigine.equals(noeud)) {
                if (noeudDejaAtteint.contains(arc.noeudExtremite)) {
                    return noeudDejaAtteint;
                } else {
                    noeudDejaAtteint.add(arc.noeudExtremite);
                    noeudDejaAtteint.addAll(getSommetsAtteignables(arc.noeudExtremite, noeudDejaAtteint));
                }
            }
        }
        return noeudDejaAtteint;
    }


    @Override
    public String toString() {
        return "Graphe{" +
                ", arcs=" + arcs.size() +
                ", noeuds=" + getNoeuds() +
                '}';
    }

    public void swapArc() {
        Noeud noeud;
        for (Arc arc : arcs) {
            noeud = arc.noeudOrigine;
            arc.noeudOrigine = arc.noeudExtremite;
            arc.noeudExtremite = noeud;
        }
    }
}
