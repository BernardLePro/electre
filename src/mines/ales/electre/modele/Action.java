package mines.ales.electre.modele;

import java.util.HashMap;
import java.util.Map;

public class Action {
    String nom;
    Map<Critere, Float> evaluation;

    public Action(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public Map<Critere, Float> getEvaluation() {
        return evaluation;
    }

    public void addCritere(Critere critere, float value) {
        if (evaluation == null)
            evaluation = new HashMap<Critere, Float>();
        evaluation.put(critere, value);
    }

    public float getValue(Critere critere) {
        return evaluation.get(critere);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Action action = (Action) o;
        return !(nom != null ? !nom.equals(action.nom) : action.nom != null);

    }

    @Override
    public int hashCode() {
        return nom != null ? nom.hashCode() : 0;
    }
}
