package mines.ales.electre.modele;

import mines.ales.electre.controller.Utils;

public class Indice {
    public final float CONCORDANCE_MOYENNE = 0.55f;
    public final float CONCORDANCE_FORTE = 0.6f;
    private final int SURCLASSEMENT_ABSENT = 1;
    private final int SURCLASSEMENT_FAIBLE = 2;
    private final int SURCLASSEMENT_FORT = 4;

    float pPlus = 0, pEgal = 0, pMoins = 0;
    float oppositionFaible, oppositionMoyenne, oppositionForte;

    public void addPplus(float value) {
        pPlus += value;
    }

    public void addpEgal(float value) {
        pEgal += value;
    }

    public void addpMoins(float value) {
        pMoins += value;
    }

    public void addOppositionFaible() {
        oppositionFaible++;
    }

    public void addOppositionMoyenne() {
        oppositionMoyenne++;
    }

    public void addOppositionForte() {
        oppositionForte++;
    }

    public float getConcordance() {
        return (pPlus + pEgal) / (pPlus + pEgal + pMoins);
    }

    public int getSurclassement() {
        Utils.logV("Opposition: faible " + oppositionFaible + ", moyenne " + oppositionMoyenne + ", forte " + oppositionForte);
        int surclassement = 0;
        if (oppositionForte > 0 || pPlus < pMoins)
            return SURCLASSEMENT_ABSENT;
        else if (oppositionMoyenne >= 0)
            surclassement += SURCLASSEMENT_FAIBLE;
        if (getConcordance() >= CONCORDANCE_MOYENNE && oppositionMoyenne > 0) ;
        else if (getConcordance() >= CONCORDANCE_MOYENNE) {
            surclassement += SURCLASSEMENT_FORT;
        }
        return surclassement;
    }


    public String getDiscordanceHumain() {
        if (oppositionForte > 0)
            return " forte";
        else if (oppositionMoyenne > 0)
            return " moyenne";
        else
            return " faible";
    }

    public String getConcordanceHumain() {
        if (pPlus >= pMoins)
            if (getConcordance() >= CONCORDANCE_FORTE)
                return " (forte)";
            else if (getConcordance() >= CONCORDANCE_MOYENNE)
                return " (moyenne)";
            else
                return " (faible)";
        return " P+ < P-";
    }

    public String getSurclassementHumain() {
        switch (getSurclassement()) {
            case SURCLASSEMENT_FAIBLE:
                return "Bf";
            case SURCLASSEMENT_ABSENT:
                return "-";
            case SURCLASSEMENT_FORT:
                return "BF";
            case SURCLASSEMENT_FAIBLE + SURCLASSEMENT_FORT:
                return "Bf+BF";
            default:
                return "???";
        }
    }

    @Override
    public String toString() {
        return "Indice{" +
                "pPlus=" + pPlus +
                ", pEgal=" + pEgal +
                ", pMoins=" + pMoins +
                ", concordance=" + getConcordance() + getConcordanceHumain() +
                ", discordance=" + getDiscordanceHumain() +
                ", surclassement= " + getSurclassementHumain() +
                '}';
    }
}
