package mines.ales.electre.modele.comparator;

public class SmallerComparator extends Comparator {
    @Override
    public boolean compare(float value1, float value2) {
        return value1 <= value2;
    }
}
