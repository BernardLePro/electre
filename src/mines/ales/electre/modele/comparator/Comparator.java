package mines.ales.electre.modele.comparator;

public abstract class Comparator {
    public abstract boolean compare(float value1, float value2);
}
