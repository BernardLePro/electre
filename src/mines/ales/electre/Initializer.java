package mines.ales.electre;

import mines.ales.electre.controller.Electre;
import mines.ales.electre.controller.Trieur;
import mines.ales.electre.controller.Utils;
import mines.ales.electre.modele.Action;
import mines.ales.electre.modele.Critere;
import mines.ales.electre.modele.Indice;
import mines.ales.electre.modele.comparator.SmallerComparator;
import mines.ales.electre.modele.graphe.Arc;
import mines.ales.electre.modele.graphe.Graphe;
import mines.ales.electre.modele.graphe.Noeud;

import java.util.ArrayList;
import java.util.List;

public class Initializer {
    private static final String SEPARATEUR = "==========================================================";
    static List<Action> actions;

    public static void init() {
        Critere prixTerrain = new Critere("Prix du terrain", 25, 100, 80);
        prixTerrain.setComparator(new SmallerComparator());

        Critere fraisTransport = new Critere("Frais de transport", 45, 1000, 800);
        fraisTransport.setComparator(new SmallerComparator());

        Critere etatEnvironnement = new Critere("Etat de l’environnement", 10, 6, 4);
        Critere residentTouche = new Critere("Résident touchés", 12, 5, 4);
        Critere competitionVocation = new Critere("Competition vocation", 8, 10, 7);

        actions = new ArrayList<Action>();

        Action action = new Action("a1");
        action.addCritere(prixTerrain, 120f);
        action.addCritere(fraisTransport, 284f);
        action.addCritere(etatEnvironnement, 5);
        action.addCritere(residentTouche, 3.5f);
        action.addCritere(competitionVocation, 18f);
        actions.add(action);

        action = new Action("a2");
        action.addCritere(prixTerrain, 150f);
        action.addCritere(fraisTransport, 269f);
        action.addCritere(etatEnvironnement, 2);
        action.addCritere(residentTouche, 4.5f);
        action.addCritere(competitionVocation, 24);
        actions.add(action);

        action = new Action("a3");
        action.addCritere(prixTerrain, 100);
        action.addCritere(fraisTransport, 413);
        action.addCritere(etatEnvironnement, 4);
        action.addCritere(residentTouche, 5.5f);
        action.addCritere(competitionVocation, 17);
        actions.add(action);

        action = new Action("a4");
        action.addCritere(prixTerrain, 60);
        action.addCritere(fraisTransport, 596);
        action.addCritere(etatEnvironnement, 6);
        action.addCritere(residentTouche, 8);
        action.addCritere(competitionVocation, 20);
        actions.add(action);

        action = new Action("a5");
        action.addCritere(prixTerrain, 30);
        action.addCritere(fraisTransport, 1321);
        action.addCritere(etatEnvironnement, 8);
        action.addCritere(residentTouche, 7.5f);
        action.addCritere(competitionVocation, 16);
        actions.add(action);

        action = new Action("a6");
        action.addCritere(prixTerrain, 80);
        action.addCritere(fraisTransport, 734);
        action.addCritere(etatEnvironnement, 5);
        action.addCritere(residentTouche, 4);
        action.addCritere(competitionVocation, 21);
        actions.add(action);

        action = new Action("a7");
        action.addCritere(prixTerrain, 45);
        action.addCritere(fraisTransport, 982);
        action.addCritere(etatEnvironnement, 7);
        action.addCritere(residentTouche, 8.5f);
        action.addCritere(competitionVocation, 13);
        actions.add(action);

        System.out.println(Electre.lanceCalcul(actions.get(0), actions.get(2)));


//        tache1();
//        tache2();
//        tache3();
    }

    public static void tache1() {
        System.out.println("\n\n\n" +
                SEPARATEUR + " Tache 1 " + SEPARATEUR);
        for (int i = 0; i < actions.size(); i++)
            for (int j = 0; j < actions.size(); j++)
                if (i != j) {
                    Utils.log("Actions: " + actions.get(i).getNom() + " et " + actions.get(j).getNom());
                    Utils.log(Electre.lanceCalcul(actions.get(i), actions.get(j)) + "\n");
                }
    }

    public static void tache2() {
        System.out.println("\n\n\n" +
                SEPARATEUR + " Tache 2 " + SEPARATEUR);
        Graphe gF = generateGF();
        Graphe gf = generateGf();

        System.out.println(SEPARATEUR + " Gf:");
        System.out.println(gf.graph2String());
        System.out.println(SEPARATEUR + " GF:");
        System.out.println(gF.graph2String());
    }

    public static void tache3() {
        System.out.println("\n\n\n" +
                SEPARATEUR + " Tache 3 " + SEPARATEUR);
        Graphe gF = generateGF();
        Graphe gf = generateGf();

        Trieur trieur = new Trieur(gf, gF);
        trieur.fusionneurNoeud();

        List<Noeud> noeudsTriDirect = trieur.triDirect(null);
        afficheTri(noeudsTriDirect);
        gF = generateGF();
        gf = generateGf();

        trieur = new Trieur(gf, gF);
        trieur.fusionneurNoeud();
        List<Noeud> noeudsTriIndirect = trieur.triInverse();
        afficheTri(noeudsTriIndirect);

    }

    public static void afficheTri(List<Noeud> noeuds) {
        String resultat = "";
        for (Noeud noeud : noeuds) {
            resultat += " => " + noeud;
        }
        System.out.println(resultat);
    }

    public static Graphe generateGF() {
        Graphe gF = new Graphe();
        Indice indice;
        for (int i = 0; i < actions.size(); i++) {
            for (int j = 0; j < actions.size(); j++)
                if (i != j) {
                    indice = Electre.lanceCalcul(actions.get(i), actions.get(j));
                    if (indice.getSurclassementHumain().equals("BF") || indice.getSurclassementHumain().equals("Bf+BF")) {
                        gF.addArc(new Arc(actions.get(i), actions.get(j)));
                    }
                }
        }
        return gF;
    }

    public static Graphe generateGf() {
        Graphe gf = new Graphe();
        Indice indice;
        for (int i = 0; i < actions.size(); i++) {
            for (int j = 0; j < actions.size(); j++)
                if (i != j) {
                    indice = Electre.lanceCalcul(actions.get(i), actions.get(j));
                    if (indice.getSurclassementHumain().equals("Bf")) {
                        gf.addArc(new Arc(actions.get(i), actions.get(j)));
                    }
                }
        }
        return gf;
    }
}
