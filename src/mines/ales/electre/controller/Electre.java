package mines.ales.electre.controller;

import mines.ales.electre.modele.Action;
import mines.ales.electre.modele.Critere;
import mines.ales.electre.modele.Indice;

import java.util.Map;

public class Electre {

    public static Indice lanceCalcul(Action action1, Action action2) {
        Indice indice = new Indice();
        indiceDeConcordance(action1, action2, indice);
        indiceDeDiscordance(action1, action2, indice);
        return indice;
    }

    public static void indiceDeConcordance(Action action1, Action action2, Indice indice) {
        Critere critere;
        float valeur;
        for (Map.Entry<Critere, Float> entry : action1.getEvaluation().entrySet()) {
            critere = entry.getKey();
            valeur = entry.getValue();
            if (valeur == action2.getValue(critere))
                indice.addpEgal(critere.getPoids());
            else {
                valeur = critere.compareConcordance(valeur, action2.getValue(critere));
                if (valeur == 0)
                    indice.addpMoins(critere.getPoids());
                else indice.addPplus(critere.getPoids());
            }
        }
    }

    public static void indiceDeDiscordance(Action action1, Action action2, Indice indice) {
        Critere critere;
        float valeur;
        for (Map.Entry<Critere, Float> entry : action1.getEvaluation().entrySet()) {
            critere = entry.getKey();
            valeur = entry.getValue();
            valeur = critere.compareDiscordance(valeur, action2.getValue(critere));
            String valeursDiscordance = valeur + " (" + critere.getSeuilDiscordanceBas() + "/" + critere.getSeuilDiscordanceHaut() + ") " + critere.getNom();
            if (valeur > critere.getSeuilDiscordanceHaut()) {
                Utils.logV("Discordance forte: " + valeursDiscordance);
                indice.addOppositionForte();
            } else if (valeur > critere.getSeuilDiscordanceBas()) {
                Utils.logV("Discordance moyenne: " + valeursDiscordance);
                indice.addOppositionMoyenne();
            } else {
                Utils.logV("Pas de discordance: " + valeursDiscordance);
                indice.addOppositionFaible();
            }
        }
    }
}
