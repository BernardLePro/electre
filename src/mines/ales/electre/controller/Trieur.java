package mines.ales.electre.controller;

import mines.ales.electre.modele.graphe.Graphe;
import mines.ales.electre.modele.graphe.Noeud;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Trieur {
    private Graphe gf;
    private Graphe gF;

    public Trieur(Graphe gf, Graphe gF) {
        this.gf = gf;
        this.gF = gF;
    }

    public void fusionneurNoeud() {
        List<Noeud> circuits = gF.getCircuits();
        gF.fusionnerNoeud(circuits);
        gf.fusionnerNoeud(circuits);
    }

    /**
     * Méthode récursive pour trier les graphes
     * @return liste triée des noeuds
     * @param noeuds mettre null ou une liste vide pour lancer le tri
     */
    public List<Noeud> triDirect(List<Noeud> noeuds) {
        if (noeuds == null)
            noeuds = new ArrayList<Noeud>();
        List<Noeud> noeudTemp = gF.getNoeudsSansArcEntrant();
        if (gF.getNoeudsSansArcs() != null)
            noeudTemp.addAll(gF.getNoeudsSansArcs());
        Noeud noeudSurclasseur = getNoeudSurclasseur(noeudTemp);
        gF.supprimerNoeud(noeudSurclasseur);
        gf.supprimerNoeud(noeudSurclasseur);
        noeuds.add(noeudSurclasseur);
        if (gF.getNoeuds().size() > 0 || gF.getNoeudsSansArcs().size() > 1)
            triDirect(noeuds);
        else if (gF.getNoeuds().size() == 0) {
            //Juste à la fin, on affiche le noeud restant
            if (gF.getNoeudsSansArcs().size() != 0)
                noeuds.addAll(gF.getNoeudsSansArcs());
        }
        return noeuds;
    }

    public List<Noeud> triInverse() {
        gF.swapArc();
        gf.swapArc();
        return triDirect(null);
    }

    public List<Noeud> triFinal(List<Noeud> triDirect, List<Noeud> triIndirect) {

        return triDirect(null);
    }


    public Noeud getNoeudSurclasseur(List<Noeud> noeuds) {
        Set<Noeud> noeudSet = new HashSet<Noeud>();
        List<Noeud> noeudsNonAtteints = new ArrayList<Noeud>();
        for (Noeud noeud : noeuds) {
            noeudSet.addAll(gf.getSommetsAtteignables(noeud, new HashSet<Noeud>()));
        }
        for (Noeud noeud : noeuds) {
            if (!noeudSet.contains(noeud))
                noeudsNonAtteints.add(noeud);
        }
        if (noeudsNonAtteints.size() == 1)
            return noeudsNonAtteints.get(0);
        else
            System.out.println("Attention: Méthode pour obtenir le noeud surclasseur dans gf ne fonctionne pas !!!");
        return null;
    }
}
