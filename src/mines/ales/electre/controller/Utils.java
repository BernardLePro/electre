package mines.ales.electre.controller;

public class Utils {
    static boolean debug = true;
    static boolean verbose = false;

    public static void log(Object object) {
        if (debug)
            System.out.println(object);
    }

    public static void logV(Object object) {
        if (verbose)
            System.out.println(object);
    }

}
